module.exports = function (grunt) {

  grunt.initConfig({
    clean: {
      dist: ['dist/*'],
      tmp: ['.tmp', '**/.DS_store']
    },
    sass: {
      dist: {
        files: [
          {'dist/css/flatkit.css': ['scss/app.scss']},
          {'dist/css/flatkit.rtl.css': ['scss/app.rtl.scss']},
          {'assets/bootstrap-rtl/dist/bootstrap-rtl.css': ['assets/bootstrap-rtl/scss/bootstrap-rtl.scss']}
        ]
      }
    },
    cssmin: {
      target: {
        files: {
          'dist/css/flatkit.min.css': ['dist/css/flatkit.css']
        }
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-sass');

  grunt.registerTask('build', [
    'clean:dist',
    'sass',
    'cssmin'
  ]);


};
